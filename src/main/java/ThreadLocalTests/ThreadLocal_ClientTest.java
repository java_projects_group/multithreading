package ThreadLocalTests;

import lombok.Value;

import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

@Value
final class Client {
    private final String name ;

    public Client(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public void sendMessage(String message)  {
        System.out.println("[" + name + "]: " + message);
    }
}

final class Worker {
    private final ThreadLocal<Client> client = new ThreadLocal<Client>() {
        @Override
        protected Client initialValue() {
            return new Client("Client_" + new Date());
        }
    };

    public void sendMessage(String message)  {
        client.get().sendMessage(message);
    }
}


public class ThreadLocal_ClientTest {

    private static void sleep(int timeout) {
        try {
            TimeUnit.SECONDS.sleep(timeout);
        } catch (final InterruptedException exc) {
            exc.printStackTrace();
        }
    }

    public static void Test() {
        Worker worker = new Worker();

        final Callable<Void> task = () -> {
            final String threadName = Thread.currentThread().getName();
            for (int i = 0 ; i < 20; ++i) {
                worker.sendMessage("Message from " + threadName);
                sleep(1);
            }
            return null;
        };

        final ExecutorService service = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 3; i++) {
            Future<Void> result = service.submit(task);
            sleep(1);
            //System.out.println(result.get());
        }
        service.shutdown();
    }

    public static void main(String ... params) {
        Test();
    }
}

